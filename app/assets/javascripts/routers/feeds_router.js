NewsReader.Routers.FeedsRouter = Backbone.Router.extend({
  initialize: function(options) {
    this.$rootEl = options.$rootEl;
    this.feeds = options.feeds;
  },

  routes: {
    '': 'index',
    '/api/feeds': 'index',
    'api/feeds/:id': 'feedShow'
  },

  index: function() {

    var indexView = new NewsReader.Views.FeedsIndex({
      collection: this.feeds
    })
    this._swapView(indexView);
  },

  feedShow: function(id) {
    var feed = this.feeds.getOrFetch(id);
    feed.fetch();
    var feedView = new NewsReader.Views.FeedShow({
      model: feed
    })
    this._swapView(feedView);
  },

  _swapView: function (newView) {
    this._currentView && this._currentView.remove();

    this._currentView = newView;
    this.$rootEl.html(newView.$el);
    newView.render();
  }
});
