NewsReader.Views.FeedsIndex = Backbone.View.extend({
  template: JST['feeds/index'],

  initialize: function (){
    this.listenToOnce(
      this.collection,
      'sync',
      this.render
    );
    this.collection.fetch();
  },

  render: function (){
    var content = this.template({feeds: this.collection});
    this.$el.html(content);

    return this;
  }

})
