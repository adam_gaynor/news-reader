NewsReader.Views.FeedShow = Backbone.View.extend({
  template: JST['feeds/show'],
  events: {
    "click .refresh-feed": "refreshFeed"
  },

  initialize: function(){
    this.listenTo(this.model, 'sync', this.render);
  },

  render: function(){

    var content = this.template({feed: this.model});
    this.$el.html(content);
    return this;
  },

  // render: function (){
  //   var content = this.template({feed: this.model});
  //   this.$el.html(content);
  //   this.attachSubviews();
  //   return this;
  // },

  refreshFeed: function (event) {
    this.model.fetch();
  }

})
