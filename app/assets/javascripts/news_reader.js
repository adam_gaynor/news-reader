window.NewsReader = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function() {
    var feeds = new NewsReader.Collections.Feeds();
    feeds.fetch({reset: true});
    new NewsReader.Routers.FeedsRouter({
      feeds: feeds,
      $rootEl: $(".feeds")
    });
    Backbone.history.start();
  }
};

$(document).ready(function(){
  NewsReader.initialize();
});
